/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.Anuncios;
import com.adsicbi.jardinapp.jpa.sessions.AnunciosFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author PC 4
 */
@Path("anuncios")
@Produces(MediaType.APPLICATION_JSON)
public class AnunciosRest {
    
    @EJB
    private AnunciosFacade ejbanunciosfacade;
            
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Anuncios anuncios){
        ejbanunciosfacade.create(anuncios);
    }
    
    
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, Anuncios anuncios){
        ejbanunciosfacade.edit(anuncios);
    }
    
    
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id){
        ejbanunciosfacade.remove(ejbanunciosfacade.find(id));
    }
     
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Anuncios> findAll(){
        return ejbanunciosfacade.findAll();
    }
            
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Anuncios findById(@PathParam("id") Integer id){
        return ejbanunciosfacade.find(id);
    }
            
}

