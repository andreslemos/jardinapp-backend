/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.Pedidos;
import com.adsicbi.jardinapp.jpa.entities.Roles;
import com.adsicbi.jardinapp.jpa.sessions.PedidosFacade;
import com.adsicbi.jardinapp.jpa.sessions.RolesFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author angela
 */
@Path("pedidos")
@Produces(MediaType.APPLICATION_JSON)
public class PedidosRest {

    @EJB
    private PedidosFacade ejbPedidosFacade;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Pedidos pedidos) {
        ejbPedidosFacade.create(pedidos);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") String id, Pedidos pedidos) {
        ejbPedidosFacade.edit(pedidos);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        ejbPedidosFacade.remove(ejbPedidosFacade.find(id));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pedidos> findAll() {
        return ejbPedidosFacade.findAll();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Pedidos findById(@PathParam("id") String id) {
        return ejbPedidosFacade.find(id);
    }
}
