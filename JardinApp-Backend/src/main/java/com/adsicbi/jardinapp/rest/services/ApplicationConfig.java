package com.adsicbi.jardinapp.rest.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author leoandresm
 */
@ApplicationPath("webresources")
public class ApplicationConfig extends Application {
    
}
