/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.JardinesInfantiles;
import com.adsicbi.jardinapp.jpa.sessions.JardinesInfantilesFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author vbn
 */
@Path("jardinesinfantiles")
@Produces(MediaType.APPLICATION_JSON)
public class JardinesInfantilesRest {
    
    @EJB
    private JardinesInfantilesFacade ejbJardinesInfantilesFacade;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(JardinesInfantiles jardindesInfantiles) {
        ejbJardinesInfantilesFacade.create(jardindesInfantiles);
    }
    
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, JardinesInfantiles jardindesInfantiles) {
         ejbJardinesInfantilesFacade.edit(jardindesInfantiles);
    }
    
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbJardinesInfantilesFacade.remove(ejbJardinesInfantilesFacade.find(id));
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<JardinesInfantiles> findAll() {
        return ejbJardinesInfantilesFacade.findAll();
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public JardinesInfantiles findById(@PathParam("id") Integer id) {
        return ejbJardinesInfantilesFacade.find(id);
    }
}
