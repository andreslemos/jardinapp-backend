package com.adsicbi.jardinapp.rest.services;


import com.adsicbi.jardinapp.jpa.entities.Inquietudes;
import com.adsicbi.jardinapp.jpa.sessions.InquietudesFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author andres
 */
@Path("inquietudes")
@Produces(MediaType.APPLICATION_JSON)
public class InquietudesRest {
    
    @EJB
    private InquietudesFacade ejbInquietudesFacade;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Inquietudes inquietudes) {
        ejbInquietudesFacade.create(inquietudes);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Inquietudes> findAll() {
        return ejbInquietudesFacade.findAll();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Inquietudes findById(@PathParam("id") String id) {
        return ejbInquietudesFacade.find(id);
    }
    
}
