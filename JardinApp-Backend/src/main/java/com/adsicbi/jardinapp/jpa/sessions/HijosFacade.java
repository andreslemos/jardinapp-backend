/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.sessions;

import com.adsicbi.jardinapp.jpa.entities.Hijos;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andres
 */
@Stateless
public class HijosFacade extends AbstractFacade<Hijos> {

    @PersistenceContext(unitName = "com.adsicbi.jardinapp_JardinApp-Backend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HijosFacade() {
        super(Hijos.class);
    }
    
}
