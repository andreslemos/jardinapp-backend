/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "jardines_infantiles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JardinesInfantiles.findAll", query = "SELECT j FROM JardinesInfantiles j"),
    @NamedQuery(name = "JardinesInfantiles.findByIdJardinInfantil", query = "SELECT j FROM JardinesInfantiles j WHERE j.idJardinInfantil = :idJardinInfantil"),
    @NamedQuery(name = "JardinesInfantiles.findByNombreJardin", query = "SELECT j FROM JardinesInfantiles j WHERE j.nombreJardin = :nombreJardin"),
    @NamedQuery(name = "JardinesInfantiles.findByDireccion", query = "SELECT j FROM JardinesInfantiles j WHERE j.direccion = :direccion"),
    @NamedQuery(name = "JardinesInfantiles.findByCelular", query = "SELECT j FROM JardinesInfantiles j WHERE j.celular = :celular"),
    @NamedQuery(name = "JardinesInfantiles.findByTelefono", query = "SELECT j FROM JardinesInfantiles j WHERE j.telefono = :telefono"),
    @NamedQuery(name = "JardinesInfantiles.findByEmail", query = "SELECT j FROM JardinesInfantiles j WHERE j.email = :email"),
    @NamedQuery(name = "JardinesInfantiles.findBySitioweb", query = "SELECT j FROM JardinesInfantiles j WHERE j.sitioweb = :sitioweb")})
public class JardinesInfantiles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_jardin_infantil")
    private Integer idJardinInfantil;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre_jardin")
    private String nombreJardin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 12)
    @Column(name = "celular")
    private String celular;
    @Size(max = 8)
    @Column(name = "telefono")
    private String telefono;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "email")
    private String email;
    @Size(max = 60)
    @Column(name = "sitioweb")
    private String sitioweb;
    @JoinTable(name = "usuarios_has_jardines_infantiles", joinColumns = {
        @JoinColumn(name = "id_jardin_infantil", referencedColumnName = "id_jardin_infantil")}, inverseJoinColumns = {
        @JoinColumn(name = "id_documento", referencedColumnName = "id_documento")})
    @ManyToMany
    private List<Usuarios> usuariosList;
    @JoinColumn(name = "id_tipo_jardines", referencedColumnName = "id_tipo_jardines")
    @ManyToOne(optional = false)
    private TiposJardines idTipoJardines;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idJardinInfantil")
    private List<Anuncios> anunciosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idJardinInfantil")
    private List<Inquietudes> inquietudesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idJardinInfantil")
    private List<Calificaciones> calificacionesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idJardinInfantil")
    private List<Hijos> hijosList;

    public JardinesInfantiles() {
    }

    public JardinesInfantiles(Integer idJardinInfantil) {
        this.idJardinInfantil = idJardinInfantil;
    }

    public JardinesInfantiles(Integer idJardinInfantil, String nombreJardin, String direccion, String email) {
        this.idJardinInfantil = idJardinInfantil;
        this.nombreJardin = nombreJardin;
        this.direccion = direccion;
        this.email = email;
    }

    public Integer getIdJardinInfantil() {
        return idJardinInfantil;
    }

    public void setIdJardinInfantil(Integer idJardinInfantil) {
        this.idJardinInfantil = idJardinInfantil;
    }

    public String getNombreJardin() {
        return nombreJardin;
    }

    public void setNombreJardin(String nombreJardin) {
        this.nombreJardin = nombreJardin;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSitioweb() {
        return sitioweb;
    }

    public void setSitioweb(String sitioweb) {
        this.sitioweb = sitioweb;
    }

    @XmlTransient
    public List<Usuarios> getUsuariosList() {
        return usuariosList;
    }

    public void setUsuariosList(List<Usuarios> usuariosList) {
        this.usuariosList = usuariosList;
    }

    public TiposJardines getIdTipoJardines() {
        return idTipoJardines;
    }

    public void setIdTipoJardines(TiposJardines idTipoJardines) {
        this.idTipoJardines = idTipoJardines;
    }

    @XmlTransient
    public List<Anuncios> getAnunciosList() {
        return anunciosList;
    }

    public void setAnunciosList(List<Anuncios> anunciosList) {
        this.anunciosList = anunciosList;
    }

    @XmlTransient
    public List<Inquietudes> getInquietudesList() {
        return inquietudesList;
    }

    public void setInquietudesList(List<Inquietudes> inquietudesList) {
        this.inquietudesList = inquietudesList;
    }

    @XmlTransient
    public List<Calificaciones> getCalificacionesList() {
        return calificacionesList;
    }

    public void setCalificacionesList(List<Calificaciones> calificacionesList) {
        this.calificacionesList = calificacionesList;
    }

    @XmlTransient
    public List<Hijos> getHijosList() {
        return hijosList;
    }

    public void setHijosList(List<Hijos> hijosList) {
        this.hijosList = hijosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idJardinInfantil != null ? idJardinInfantil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JardinesInfantiles)) {
            return false;
        }
        JardinesInfantiles other = (JardinesInfantiles) object;
        if ((this.idJardinInfantil == null && other.idJardinInfantil != null) || (this.idJardinInfantil != null && !this.idJardinInfantil.equals(other.idJardinInfantil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.JardinesInfantiles[ idJardinInfantil=" + idJardinInfantil + " ]";
    }
    
}
